# connectFour

A very simple REST API that acts as a simple gameserver. The frontend can be implemented in basically anything. This repo will probably contain a web frontend sometime soon.

# Kubernetes

One of the main aspects for me is the integration into k8s and to create a complete pipeline that manages the testing and deployment of the game into a kubernetes cluster.