package main

import (
	game2 "gitlab.com/svenfinke/connectFour/src/game"
)

type StartGameResponse struct {
	GameId    string
	GameState game2.GameState
}

type MoveRequest struct {
	GameId string
	PlayerIdx int
	ColIdx int
}
type MoveResponse struct {
	GameId    string
	GameState game2.GameState
}
type ListGamesResponse struct {
	Games map[string]game2.GameState
}

type GetGameDetailResponse struct {
	GameState game2.GameState
}