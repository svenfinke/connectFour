package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/rs/xid"
	game2 "gitlab.com/svenfinke/connectFour/src/game"
	"io/ioutil"
	"net/http"
)

// Replace this "local storage" with persistent files or a key-value storage
var (
	games map[string]game2.GameState
)

// Start is launching the game-server and providing access to the Handlers
func Start(){
	games = make(map[string]game2.GameState)

	r := mux.NewRouter()
	r.HandleFunc("/start", startGameHandler)
	r.HandleFunc("/", listGamesHandler)
	r.HandleFunc("/move", moveHandler)
	r.HandleFunc("/details/{id}", getGameDetailsHandler)
	http.Handle("/", r)

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		panic(err)
	}
}

func startGameHandler(w http.ResponseWriter, r *http.Request){
	newGameState := game2.NewGame()
	gameId := xid.New().String()

	games[gameId] = newGameState

	response := StartGameResponse{
		GameId:    gameId,
		GameState: newGameState,
	}

	jsonResponse, err := json.Marshal(response)
	if err != nil {
		panic(err)
	}
	_, err = w.Write(jsonResponse)
	if err != nil {
		panic(err)
	}
}

func listGamesHandler(w http.ResponseWriter, r *http.Request){
	response := ListGamesResponse{
		Games: games,
	}

	jsonResponse, err := json.Marshal(response)
	if err != nil {
		panic(err)
	}
	_, err = w.Write(jsonResponse)
	if err != nil {
		panic(err)
	}
}

func getGameDetailsHandler(w http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
	response := GetGameDetailResponse{
		GameState: games[vars["Id"]],
	}

	jsonResponse, err := json.Marshal(response)
	if err != nil {
		panic(err)
	}
	_, err = w.Write(jsonResponse)
	if err != nil {
		panic(err)
	}
}

func moveHandler(w http.ResponseWriter, r *http.Request){
	mr := MoveRequest{}
	content,_ := ioutil.ReadAll(r.Body)
	json.Unmarshal(content, &mr)

	fmt.Println(mr)

	g := game2.Game{}
	gs := games[mr.GameId]
	g.Move(&gs, mr.PlayerIdx, mr.ColIdx)
	games[mr.GameId] = gs

	response := MoveResponse{
		GameId:    mr.GameId,
		GameState: gs,
	}

	jsonResponse, err := json.Marshal(response)
	if err != nil {
		panic(err)
	}
	_, err = w.Write(jsonResponse)
	if err != nil {
		panic(err)
	}
}