package game

type GameState struct {
	PlayerCount int
	PlayedRounds int
	GameFinished bool
	WinnerIdx int
	Grid [][]int
}

type Game struct {
}