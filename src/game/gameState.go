package game

func newGameState() GameState{
	gs := GameState{
		PlayerCount:  2,
		PlayedRounds: 0,
		GameFinished: false,
		WinnerIdx:    -1,
		Grid: [][]int{},
	}

	for x := 0; x < 7; x++ {
		for y := 0; y < 6; y++ {
			if len(gs.Grid) == x {
				gs.Grid = append(gs.Grid, []int{})
			}
			if len(gs.Grid[x]) == y {
				gs.Grid[x] = append(gs.Grid[x], -1)
			}

			gs.Grid[x][y] = -1
		}
	}

	return gs
}