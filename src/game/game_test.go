package game

import (
	"reflect"
	"testing"
)

func TestNewGame(t *testing.T) {
	t.Run("Verify if NewGame returns result of newGameState", func(t *testing.T){
		want := newGameState()
		if got := NewGame(); !reflect.DeepEqual(got, want) {
			t.Errorf("NewGame() = %v, want %v", got, want)
		}
	})
}

func TestGame_Move(t *testing.T) {
	game := Game{}

	row5SecondTimeState := newGameState()
	row5SecondTimeState.Grid[5][0] = 0
	row5SecondTimeState.Grid[5][1] = 1

	row4ThirdTimeState := newGameState()
	row4ThirdTimeState.Grid[4][0] = 0
	row4ThirdTimeState.Grid[4][1] = 1
	row4ThirdTimeState.Grid[4][2] = 0

	t.Run("Test 2 Time Move in Column 5", func(t *testing.T) {
		gs := newGameState()
		err := game.Move(&gs, 0, 5)
		if err != nil {
			t.Errorf(err.Error())
		}
		err = game.Move(&gs, 1, 5)
		if err != nil {
			t.Errorf(err.Error())
		}
		if !reflect.DeepEqual(gs.Grid, row5SecondTimeState.Grid) {
			t.Errorf("Move on Column 5 failed. Expected '%v' to equal '%v'", gs.Grid[5], row5SecondTimeState.Grid[5])
		}
	})

	t.Run("Test 3 Time Move in Column 4", func(t *testing.T) {
		gs := newGameState()
		err := game.Move(&gs, 0, 4)
		if err != nil {
			t.Errorf(err.Error())
		}
		err = game.Move(&gs, 1, 4)
		if err != nil {
			t.Errorf(err.Error())
		}
		err = game.Move(&gs, 0, 4)
		if err != nil {
			t.Errorf(err.Error())
		}

		if !reflect.DeepEqual(gs.Grid, row4ThirdTimeState.Grid) {
			t.Errorf("Move on Column 5 failed. Expected '%v' to equal '%v'", gs.Grid[4], row4ThirdTimeState.Grid[4])
		}
	})
}

func TestGame_isMoveValid(t *testing.T) {
	t.Run("Check invalid moves being registered as invalid.", func(t *testing.T) {
		t.Run("First move from wrong player", func(t *testing.T) {
			game := Game{}
			gs := newGameState()
			if game.isMoveValid(&gs, 1, 4) {
				t.Errorf("The first move can be made by the wrong player.")
			}
		})

		t.Run("Two successive moves by the same player", func(t *testing.T) {
			game := Game{}
			gs := newGameState()
			err := game.Move(&gs, 0, 1)
			if err != nil {
				t.Errorf(err.Error())
			}
			if game.isMoveValid(&gs, 0, 4) {
				t.Errorf("The same player can make two successive moves.")
			}
		})

		t.Run("Set a stone into a full row.", func(t *testing.T) {
			game := Game{}
			gs := newGameState()
			for x := 0; x < 6; x++ {
				err := game.Move(&gs, x % 2, 1)
				if err != nil {
					t.Errorf(err.Error())
				}
			}
			if game.isMoveValid(&gs, 1, 4) {
				t.Errorf("A full row is not registered as error.")
			}
		})
	})

	t.Run("Check valid moves being registered as valid.", func(t *testing.T) {
		t.Run("First Move done by the right player", func(t *testing.T) {
			game := Game{}
			gs := newGameState()
			if !game.isMoveValid(&gs, 0, 4) {
				t.Errorf("Failed")
			}
		})

		t.Run("Successive Moves done by the right players", func(t *testing.T) {
			game := Game{}
			gs := newGameState()
			err := game.Move(&gs, 0, 1)
			if err != nil {
				t.Errorf(err.Error())
			}
			if !game.isMoveValid(&gs, 1, 4) {
				t.Errorf("Failed.")
			}
		})
	})
}