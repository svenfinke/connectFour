package game

import (
	"errors"
)

func NewGame() GameState {
	return newGameState()
}

func (g *Game) Move (gs *GameState, playerIdx int, columnIdx int) error {
	col := gs.Grid[columnIdx]
	for y := 0; y < 6; y++ {
		if col[y] == -1 {
			gs.Grid[columnIdx][y] = playerIdx
			gs.PlayedRounds++
			return nil
		}
	}

	return errors.New("Move could not be made. Is the column already full?")
}

func (g *Game) isMoveValid(gs *GameState, playerIdx int, columnIdx int) bool {
	if gs.PlayedRounds % 2 != playerIdx {
		return false
	}

	return true
}