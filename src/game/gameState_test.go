package game

import (
	"testing"
)

func Test_newGameState(t *testing.T) {
	t.Run("Check if all grid elements are -1", func(t *testing.T) {
		gs := newGameState()
		for x, col := range gs.Grid {
			for y, el := range col {
				if el != -1 {
					t.Errorf("Element (%v,%v) should be %v, but is %v.", x, y, -1, el)
				}
			}
		}
	})
}