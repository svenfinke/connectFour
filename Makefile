all: test run

test:
	@cd src/ && go test -coverprofile cp.out ../...
test-verbose:
	@cd src/ && go test -v -coverprofile cp.out ../...
run:
	@cd src/server && go run .

build_docker:
	@cd src/server && zip ../../container/server/server.zip *
	@cd container/server && docker build -t connectfour:server .
	@rm container/server/server.zip
run_docker:
	@docker run -p "80:8080" -d connectfour:server
deploy:
	@kubectl apply -f k8s/server.yaml